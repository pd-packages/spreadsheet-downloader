# Changelog

---

## [v2.1.5](https://github.com/Playdarium/spreadsheet-downloader/releases/tag/v2.1.5)

### Changes

- Async serialize in `SpreadsheetDownloader`

---

## [v2.1.4](https://github.com/Playdarium/spreadsheet-downloader/releases/tag/v2.1.4)

### Fixed

- Download `ISpreadsheetLoader` with dependencies using `SpreadsheetDownloader`

---

## [v2.1.3](https://github.com/Playdarium/spreadsheet-downloader/releases/tag/v2.1.3)

### Fixed

- Throw exception in download or serialization process

---

## [v2.1.2](https://github.com/Playdarium/spreadsheet-downloader/releases/tag/v2.1.2)

### Fixed

- Move draw buttons to method in `SpreadsheetDownloader` editor

---

## [v2.1.1](https://github.com/Playdarium/spreadsheet-downloader/releases/tag/v2.1.1)

### Added

- Download `ISpreadsheetLoader` with dependencies

---

## [v2.1.0](https://github.com/Playdarium/spreadsheet-downloader/releases/tag/v2.1.0)

### Added

- Dependent pages in `ISpreadsheetLoader`

### Changed

- Use `async / await` in spreadsheet loaders instead `Coroutines`

---

## [v2.0.5](https://github.com/Playdarium/spreadsheet-downloader/releases/tag/v2.0.5)

### Fixed

- Download progress bar dispose

---

## [v2.0.4](https://github.com/Playdarium/spreadsheet-downloader/releases/tag/v2.0.4)

### Fixed

- Download progress bar dispose

---

## [v2.0.3](https://github.com/Playdarium/spreadsheet-downloader/releases/tag/v2.0.3)

### Fixed

- Download progress bar dispose

---

## [v2.0.2](https://github.com/Playdarium/spreadsheet-downloader/releases/tag/v2.0.2)

### Fixed

- In Utilities.ReadCsv add change `\r\n` replace to `\n`

---

## [v2.0.1](https://github.com/Playdarium/spreadsheet-downloader/releases/tag/v2.0.1)

### Added

- ReadAsJArray in SpreadsheetDownloader

---

## [v2.0.0](https://github.com/Playdarium/spreadsheet-downloader/releases/tag/v2.0.0)

### Changes

- Download google sheets as CSV file using list id
- Read multiple filed with the same name as array

### Removed

- Google App Script

---

## [v1.1.2](https://github.com/Playdarium/spreadsheet-downloader/releases/tag/v1.1.2)

### Changes

- Move to GitLab

---

## [v1.1.1](https://github.com/Playdarium/spreadsheet-downloader/releases/tag/v1.1.1)

### Fixed

- Start download coroutine ownerless

---

## [v1.1.0](https://github.com/Playdarium/spreadsheet-downloader/releases/tag/v1.1.0)

### Added

- Using `Schema` in download google spreadsheet script

### Fixed

- Process exceptions in serialization method

---

## [v1.0.0](https://github.com/Playdarium/spreadsheet-downloader/releases/tag/v1.0.0)

### Added

- Package init

---

## [v0.0.0](https://github.com/Playdarium/spreadsheet-downloader/releases/tag/v0.0.0)

### Added

- For new features.

### Changed

- For changes in existing functionality.

### Deprecated

- For soon-to-be removed features.

### Removed

- For now removed features.

### Fixed

- For any bug fixes.

### Security

- In case of vulnerabilities.
