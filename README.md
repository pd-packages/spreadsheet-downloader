# Spreadsheet Downloader

[![NPM package](https://img.shields.io/npm/v/com.playdarium.unity.spreadsheet-downloader?logo=npm&logoColor=fff&label=NPM+package&color=limegreen)](https://www.npmjs.com/package/com.playdarium.unity.spreadsheet-downloader)
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

## Installing

Using the native Unity Package Manager introduced in 2017.2, you can add this library as a package by modifying your
`manifest.json` file found at `/ProjectName/Packages/manifest.json` to include it as a dependency. See the example below
on how to reference it.

### Install via OpenUPM

The package is available on the [npmjs](https://www.npmjs.com/package/com.playdarium.unity.spreadsheet-downloader)
registry.

#### Add registry scope

```
{
  "dependencies": {
    ...
  },
  "scopedRegistries": [
    {
      "name": "Playdarium",
      "url": "https://registry.npmjs.org",
      "scopes": [
        "com.playdarium.unity"
      ]
    }
  ]
}
```

#### Add package in PackageManager

Open `Window -> Package Manager` choose `Packages: My Regestries` and install package

### Install via GIT URL

```
"com.playdarium.unity.spreadsheet-downloader": "https://gitlab.com/pd-packages/spreadsheet-downloader.git#upm"
```
