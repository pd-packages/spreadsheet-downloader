using UnityEngine;

[CreateAssetMenu(menuName = "Databases/Test/" + nameof(NotExistDatabase), fileName = nameof(NotExistDatabase))]
public class NotExistDatabase : ScriptableObject
{
}