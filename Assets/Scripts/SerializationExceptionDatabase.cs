using UnityEngine;

[CreateAssetMenu(menuName = "Databases/Test/" + nameof(SerializationExceptionDatabase),
	fileName = nameof(SerializationExceptionDatabase))]
public class SerializationExceptionDatabase : ScriptableObject
{
}