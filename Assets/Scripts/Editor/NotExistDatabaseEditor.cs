using System.Threading.Tasks;
using Playdarium.SpreadsheetDownloader.Helpers;
using UnityEditor;
using UnityEngine;

namespace Editor
{
	[CustomEditor(typeof(NotExistDatabase))]
	public class NotExistDatabaseEditor : SingleSpreadsheetDownloader
	{
		protected override string PageId => "NotExist";

		protected override Task Serialize(string json)
		{
			Debug.Log($"[{nameof(NotExistDatabaseEditor)}] \n{json}");
			return Task.CompletedTask;
		}
	}
}