using System.Threading.Tasks;
using Playdarium.SpreadsheetDownloader.Helpers;
using UnityEditor;
using UnityEngine;

namespace Editor
{
	[CustomEditor(typeof(TestDatabase))]
	public class TestDatabaseEditor : SingleSpreadsheetDownloader
	{
		protected override string PageId => "71337555";

		protected override Task Serialize(string json)
		{
			Debug.Log($"[{nameof(TestDatabaseEditor)}] \n{json}");
			return Task.CompletedTask;
		}
	}
}