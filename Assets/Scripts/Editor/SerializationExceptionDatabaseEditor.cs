using System;
using System.Threading.Tasks;
using Playdarium.SpreadsheetDownloader.Helpers;
using UnityEditor;

namespace Editor
{
	[CustomEditor(typeof(SerializationExceptionDatabase))]
	public class SerializationExceptionDatabaseEditor : SingleSpreadsheetDownloader
	{
		protected override string PageId => "71337555";

		protected override Task Serialize(string json)
		{
			throw new Exception($"[{nameof(NotExistDatabaseEditor)}] Test exception");
		}
	}
}