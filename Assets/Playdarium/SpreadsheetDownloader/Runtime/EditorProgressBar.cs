#if UNITY_EDITOR

using System;
using UnityEditor;
using UnityEngine;

namespace Playdarium.SpreadsheetDownloader.Runtime
{
	public class EditorProgressBar : IDisposable
	{
		private readonly string _title;

		public event Action OnCancel;

		private bool _isDisposed;

		public EditorProgressBar(string title)
		{
			_title = title;
		}

		public void Refresh(string message, float progress = 0)
		{
			var canceled = EditorUtility.DisplayCancelableProgressBar(_title, message, Mathf.Clamp01(progress));
			if (!canceled)
				return;

			OnCancel?.Invoke();
			Dispose();
		}


		public void Dispose()
		{
			if (_isDisposed)
				return;

			_isDisposed = true;
			OnCancel = null;
			EditorUtility.ClearProgressBar();
		}
	}
}
#endif