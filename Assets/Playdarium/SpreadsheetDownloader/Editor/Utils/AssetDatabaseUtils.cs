using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using Object = UnityEngine.Object;

namespace Playdarium.SpreadsheetDownloader.Utils
{
	internal static class AssetDatabaseUtils
	{
		/// <summary>
		/// Fetch all assets of type `T`
		/// </summary>
		/// <typeparam name="T">the type to retrieve, must inherit from ScriptableObject and implement IHasDefault and ICustomSettings</typeparam>
		/// <returns>All the assets of that type on the project</returns>
		public static T[] GetAll<T>()
			where T : Object
			=> GetAll(typeof(T)).Cast<T>().ToArray();

		public static IEnumerable<Object> GetAll(Type type)
			=> AssetDatabase.FindAssets("t:" + type.Name)
				.Select(AssetDatabase.GUIDToAssetPath)
				.Select(path => AssetDatabase.LoadAssetAtPath(path, type));
	}
}