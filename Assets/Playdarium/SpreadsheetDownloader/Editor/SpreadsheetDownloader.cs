﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Playdarium.SpreadsheetDownloader.Helpers;
using Playdarium.SpreadsheetDownloader.Runtime;
using Playdarium.SpreadsheetDownloader.Runtime.Utils;
using Playdarium.SpreadsheetDownloader.Utils;
using UnityEditor;
using UnityEngine;
using UnityEngine.Networking;

namespace Playdarium.SpreadsheetDownloader
{
	public abstract class SpreadsheetDownloader : Editor, ISpreadsheetLoader
	{
		private const string GOOGLE_EXPORT_LIST_URL =
			"https://docs.google.com/spreadsheets/d/{0}/export?format=csv&id={0}&gid={1}";

		private static readonly Regex HeaderRegex = new("^\\w+$");

		private CancellationTokenSource _cancellationTokenSource;

		public abstract string[] PageIds { get; }

		public virtual string[] DependencyPageIds { get; } = Array.Empty<string>();

		protected virtual void OnDisable()
		{
			if (_cancellationTokenSource is { IsCancellationRequested: false })
			{
				_cancellationTokenSource.Cancel();
				_cancellationTokenSource.Dispose();
				_cancellationTokenSource = null;
			}
		}

		public override void OnInspectorGUI()
		{
			DrawDownloadButtons();

			base.OnInspectorGUI();
		}

		protected void DrawDownloadButtons()
		{
			if (GUILayout.Button("Download"))
				ProcessTask(DownloadAndSerialize);

			if (GUILayout.Button("Download With Dependencies"))
				ProcessTask(DownloadAndSerializeWithDependencies);
		}

		private async Task DownloadAndSerializeWithDependencies(CancellationTokenSource tokenSource)
		{
			var cancellationToken = tokenSource.Token;
			using var progressBar = new EditorProgressBar("Download");
			progressBar.OnCancel += tokenSource.Cancel;

			var downloadChain = SpreadsheetDownloaderUtils.CreateDownloadChain(this);
			foreach (var loader in downloadChain)
			{
				cancellationToken.ThrowIfCancellationRequested();

				await loader.DownloadAndSerializeAsync(progressBar, cancellationToken);
			}
		}

		private async Task DownloadAndSerialize(CancellationTokenSource tokenSource)
		{
			using var progressBar = new EditorProgressBar("Download");
			progressBar.OnCancel += tokenSource.Cancel;

			await DownloadAndSerializeAsync(progressBar, tokenSource.Token);
			Debug.Log($"[{GetType().Name}] Download complete success");
		}

		private async void ProcessTask(Func<CancellationTokenSource, Task> downloadTask)
		{
			if(_cancellationTokenSource != null)
				return;
			
			try
			{
				_cancellationTokenSource = new CancellationTokenSource();
				await downloadTask(_cancellationTokenSource);
				Debug.Log($"[{GetType().Name}] Download complete success");
			}
			catch (TaskCanceledException)
			{
				EditorUtility.DisplayDialog(
					"Spreadsheet Downloader",
					"Download task CANCELED",
					"Ok"
				);
			}
			catch (Exception e)
			{
				Debug.LogException(e, target);

				EditorUtility.DisplayDialog(
					"Spreadsheet Downloader",
					"Download completed with EXCEPTION",
					"Ok"
				);
			}
			finally
			{
				_cancellationTokenSource.Dispose();
				_cancellationTokenSource = null;
			}
		}

		public abstract Task DownloadAndSerializeAsync(
			EditorProgressBar progressBar,
			CancellationToken cancellationToken
		);

		protected async Task<string> LoadAsync(string pageId, CancellationToken cancellationToken)
		{
			var assets = AssetDatabase.FindAssets($"t:{nameof(SpreadsheetSettings)}");
			if (assets.Length == 0)
				throw new Exception($"[{GetType().Name}] Cannot find '{nameof(SpreadsheetSettings)}'");

			var settings = AssetDatabase.LoadAssetAtPath<SpreadsheetSettings>(AssetDatabase.GUIDToAssetPath(assets[0]));
			var url = string.Format(GOOGLE_EXPORT_LIST_URL, settings.spreadsheet, pageId);
			var jObject = new JObject();

			using var unityWebRequest = UnityWebRequest.Get(url);
			await unityWebRequest.SendWebRequest();

			cancellationToken.ThrowIfCancellationRequested();

			if (!string.IsNullOrEmpty(unityWebRequest.error))
				throw new Exception($"[{GetType().Name}] Download page {pageId} error: {unityWebRequest.error}");

			var text = unityWebRequest.downloadHandler.text;
			var rawTable = Utilities.ReadCsv(text);
			var headers = rawTable[0];

			var jArray = new JArray();
			for (var i = 1; i < rawTable.Count; i++)
			{
				var content = rawTable[i];
				var item = new JObject();
				for (var j = 0; j < headers.Length; j++)
				{
					var header = headers[j];
					var isArray = headers.Count(s => s == header) > 1;
					if (!HeaderRegex.IsMatch(header))
						continue;

					if (isArray)
					{
						if (!item.TryGetValue(header, out var jToken))
						{
							jToken = new JArray();
							item.Add(header, jToken);
						}

						((JArray)jToken).Add(content[j]);
					}
					else
						item[header] = content[j];
				}

				jArray.Add(item);
			}

			jObject.Add("array", jArray);

			return jObject.ToString();
		}

		protected static T[] Read<T>(string json) => JsonUtility.FromJson<ArrayJson<T>>(json).array;

		protected static JArray ReadAsJArray(string json) => JObject.Parse(json).TryGetValue("array", out var jArray)
			? jArray as JArray
			: new JArray();

		[Serializable]
		private class ArrayJson<T>
		{
			public T[] array;
		}
	}
}