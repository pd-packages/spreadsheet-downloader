using System.Collections.Generic;
using System.Text;

namespace Playdarium.SpreadsheetDownloader.Helpers
{
	public static class Utilities
	{
		public static List<string[]> ReadCsv(string csvText)
		{
			csvText = csvText.Replace("\r\n", "\n");

			var rows = new List<string[]>();

			var currentField = new StringBuilder();
			var currentRow = new List<string>();
			var insideQuotes = false;

			foreach (var c in csvText)
			{
				if (c == '"')
				{
					insideQuotes = !insideQuotes;
				}
				else if (c == '\n' && !insideQuotes)
				{
					currentRow.Add(currentField.ToString());
					rows.Add(currentRow.ToArray());
					currentRow.Clear();
					currentField.Clear();
				}
				else if (c == ',' && !insideQuotes)
				{
					currentRow.Add(currentField.ToString());
					currentField.Clear();
				}
				else
				{
					currentField.Append(c);
				}
			}

			// Add the last field and row
			currentRow.Add(currentField.ToString());
			rows.Add(currentRow.ToArray());

			return rows;
		}
	}
}