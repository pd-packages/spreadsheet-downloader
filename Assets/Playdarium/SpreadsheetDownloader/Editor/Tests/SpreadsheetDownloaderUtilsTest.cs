using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;
using Playdarium.SpreadsheetDownloader.Exceptions;
using Playdarium.SpreadsheetDownloader.Runtime;
using Playdarium.SpreadsheetDownloader.Utils;

namespace Playdarium.SpreadsheetDownloader.Tests
{
	public class SpreadsheetDownloaderUtilsTest
	{
		[Test]
		public void CorrectLoaderChain_Download_Success()
		{
			var expectedPages = new ISpreadsheetLoader[]
			{
				SpLoader2,
				SpLoader4,
				SpLoader3,
				SpLoader1,
			};

			var downloadChain = SpreadsheetDownloaderUtils.CreateDownloadChain(Correct[0], Correct);
			Assert.That(expectedPages.SequenceEqual(downloadChain));
		}

		[Test]
		public void CorrectDuplicatedLoaderChain_Download_Success()
		{
			var expectedPages = new ISpreadsheetLoader[]
			{
				SpLoader2,
				SpLoader4,
				SpLoader3,
				SpLoader1,
			};

			var downloadChain = SpreadsheetDownloaderUtils.CreateDownloadChain(CorrectDuplicated[0], CorrectDuplicated);
			Assert.That(expectedPages.SequenceEqual(downloadChain));
		}

		[Test]
		public void CorrectBranchedLoaderChain_Download_Success()
		{
			var expectedPages = new ISpreadsheetLoader[]
			{
				SpLoader2,
				SpLoaderBranched,
				SpLoader3,
				SpLoader1,
			};

			var downloadChain = SpreadsheetDownloaderUtils.CreateDownloadChain(CorrectBranched[0], CorrectBranched);
			Assert.That(expectedPages.SequenceEqual(downloadChain));
		}

		[Test]
		public void CircularLoaderChain_Download_Success()
		{
			Assert.Throws<SpreadsheetDownloaderCircularDependencyException>(CreateCircularChain);

			return;

			void CreateCircularChain()
			{
				SpreadsheetDownloaderUtils.CreateDownloadChain(Circular[0], Circular);
			}
		}

		private static readonly SpreadsheetLoader1 SpLoader1 = new();
		private static readonly SpreadsheetLoader2 SpLoader2 = new();
		private static readonly SpreadsheetLoader3 SpLoader3 = new();
		private static readonly SpreadsheetLoader4 SpLoader4 = new();
		private static readonly SpreadsheetLoaderBranched SpLoaderBranched = new();

		private static readonly ISpreadsheetLoader[] Correct =
		{
			SpLoader1,
			SpLoader2,
			SpLoader3,
			SpLoader4,
		};

		private static readonly ISpreadsheetLoader[] CorrectDuplicated =
		{
			SpLoader1,
			SpLoader2,
			SpLoader2,
			SpLoader3,
			SpLoader3,
			SpLoader4,
			SpLoader4,
		};

		private static readonly ISpreadsheetLoader[] CorrectBranched =
		{
			SpLoader1,
			SpLoader2,
			SpLoader3,
			SpLoaderBranched,
		};

		private static readonly ISpreadsheetLoader[] Circular =
		{
			SpLoader1,
			SpLoader2,
			SpLoader3,
			new SpreadsheetLoaderCircular(),
		};

		private class SpreadsheetLoader1 : ASpreadsheetLoaderStub
		{
			public SpreadsheetLoader1() : base(new[] { "1" }, new[] { "2", "3" })
			{
			}
		}

		private class SpreadsheetLoader2 : ASpreadsheetLoaderStub
		{
			public SpreadsheetLoader2() : base(new[] { "2" }, Array.Empty<string>())
			{
			}
		}

		private class SpreadsheetLoader3 : ASpreadsheetLoaderStub
		{
			public SpreadsheetLoader3() : base(new[] { "3" }, new[] { "4" })
			{
			}
		}

		private class SpreadsheetLoader4 : ASpreadsheetLoaderStub
		{
			public SpreadsheetLoader4() : base(new[] { "4" }, Array.Empty<string>())
			{
			}
		}

		private class SpreadsheetLoaderBranched : ASpreadsheetLoaderStub
		{
			public SpreadsheetLoaderBranched() : base(new[] { "4" }, new[] { "2" })
			{
			}
		}

		private class SpreadsheetLoaderCircular : ASpreadsheetLoaderStub
		{
			public SpreadsheetLoaderCircular() : base(new[] { "4" }, new[] { "1" })
			{
			}
		}

		private abstract class ASpreadsheetLoaderStub : ISpreadsheetLoader
		{
			protected ASpreadsheetLoaderStub(string[] pageIds, string[] dependencyPageIds)
			{
				PageIds = pageIds;
				DependencyPageIds = dependencyPageIds;
			}

			public string[] PageIds { get; }

			public string[] DependencyPageIds { get; }

			public Task DownloadAndSerializeAsync(EditorProgressBar progressBar, CancellationToken cancellationToken)
				=> Task.CompletedTask;

			public override string ToString()
				=> $"{nameof(PageIds)}: {string.Join(", ", PageIds)}"
				   + $", {nameof(DependencyPageIds)}: {string.Join(", ", DependencyPageIds)}";
		}
	}
}