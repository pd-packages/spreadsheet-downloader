﻿using UnityEngine;

namespace Playdarium.SpreadsheetDownloader
{
	[CreateAssetMenu(menuName = "Settings/Spreadsheet", fileName = nameof(SpreadsheetSettings))]
	public class SpreadsheetSettings : ScriptableObject
	{
		[Tooltip("Table code '1pt8x...88lp8we5rk'")]
		public string spreadsheet = "";
		public int timeout = 20;
	}
}