using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEditor;
using Object = UnityEngine.Object;

namespace Playdarium.SpreadsheetDownloader.Utils
{
	internal static class AssemblyUtils
	{
		public static IEnumerable<ISpreadsheetLoader> CreateSpreadsheetLoadersEditors(IEnumerable<Object> objects)
			=> objects.Select(Editor.CreateEditor).OfType<ISpreadsheetLoader>();
		
		public static IEnumerable<Object> GetObjectsWithSpreadsheetLoaders()
			=> AppDomain.CurrentDomain.GetAssemblies()
				.SelectMany(s => s.GetTypes())
				.Where(s => !s.IsAbstract && typeof(ISpreadsheetLoader).IsAssignableFrom(s))
				.Select(s => s.GetCustomAttribute<CustomEditor>())
				.Where(attr => attr != null)
				.Select(attr => attr.GetType().GetField(
					"m_InspectedType",
					BindingFlags.Default | BindingFlags.Instance | BindingFlags.NonPublic
				)?.GetValue(attr))
				.OfType<Type>()
				.SelectMany(AssetDatabaseUtils.GetAll);
	}
}