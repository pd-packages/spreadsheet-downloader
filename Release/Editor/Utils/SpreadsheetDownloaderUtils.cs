using System.Collections.Generic;
using System.Linq;
using Playdarium.SpreadsheetDownloader.Exceptions;

namespace Playdarium.SpreadsheetDownloader.Utils
{
	internal static class SpreadsheetDownloaderUtils
	{
		public static List<ISpreadsheetLoader> CreateDownloadChain(ISpreadsheetLoader targetLoader)
		{
			var objects = AssemblyUtils.GetObjectsWithSpreadsheetLoaders();
			var spreadsheetLoaders = AssemblyUtils.CreateSpreadsheetLoadersEditors(objects);
			var targetInstance = spreadsheetLoaders.Single(s => s.GetType() == targetLoader.GetType());
			return CreateDownloadChain(targetInstance, spreadsheetLoaders);
		}

		public static List<ISpreadsheetLoader> CreateDownloadChain(
			ISpreadsheetLoader targetLoader,
			IEnumerable<ISpreadsheetLoader> spreadsheetLoaders
		)
		{
			var roots = new List<ISpreadsheetLoader>();
			CollectRoots(new List<ISpreadsheetLoader>(), roots, targetLoader, spreadsheetLoaders);
			return roots;
		}

		private static void CollectRoots(
			List<ISpreadsheetLoader> iteratedLoaders,
			List<ISpreadsheetLoader> rootLoaders,
			ISpreadsheetLoader iteratedLoader,
			IEnumerable<ISpreadsheetLoader> spreadsheetLoaders
		)
		{
			if (iteratedLoaders.Contains(iteratedLoader))
			{
				var loaderTypeNames = iteratedLoaders.Select(s => s.ToString())
					.Distinct();

				throw new SpreadsheetDownloaderCircularDependencyException(
					$"[{nameof(SpreadsheetDownloaderUtils)}] Cannot complete loading chain"
					+ $" because '{iteratedLoader.GetType()}' has circular dependencies\n"
					+ $"Dependency chain:"
					+ $"\n{string.Join("\n", loaderTypeNames)}"
				);
			}

			var dependencyPageIds = iteratedLoader.DependencyPageIds;
			if (dependencyPageIds.Length == 0)
			{
				if (!rootLoaders.Contains(iteratedLoader))
					rootLoaders.Add(iteratedLoader);

				return;
			}

			iteratedLoaders.Add(iteratedLoader);

			foreach (var dependencyPageId in dependencyPageIds)
			{
				foreach (var loader in spreadsheetLoaders)
				{
					if (!loader.PageIds.Contains(dependencyPageId))
						continue;

					CollectRoots(iteratedLoaders, rootLoaders, loader, spreadsheetLoaders);
				}
			}

			if (!rootLoaders.Contains(iteratedLoader))
				rootLoaders.Add(iteratedLoader);

			iteratedLoaders.Remove(iteratedLoader);
		}
	}
}