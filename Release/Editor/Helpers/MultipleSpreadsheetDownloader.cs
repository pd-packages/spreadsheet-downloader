using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Playdarium.SpreadsheetDownloader.Runtime;

namespace Playdarium.SpreadsheetDownloader.Helpers
{
	public abstract class MultipleSpreadsheetDownloader : SpreadsheetDownloader
	{
		public override abstract string[] PageIds { get; }

		public override async Task DownloadAndSerializeAsync(
			EditorProgressBar progressBar1,
			CancellationToken cancellationToken
		)
		{
			var pageJsons = new Dictionary<string, string>();
			using var progressBar = new EditorProgressBar("Download");

			for (var index = 0; index < PageIds.Length; index++)
			{
				cancellationToken.ThrowIfCancellationRequested();

				var pageId = PageIds[index];
				progressBar.Refresh($"Table: {pageId}...", index / (float)PageIds.Length);

				var json = await LoadAsync(pageId, cancellationToken);
				pageJsons.Add(pageId, json);
			}

			await Serialize(pageJsons);
			serializedObject.ApplyModifiedProperties();
		}

		protected abstract Task Serialize(Dictionary<string, string> jsonByTables);
	}
}