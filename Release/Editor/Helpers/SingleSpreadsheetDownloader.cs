using System.Collections.Generic;
using System.Threading.Tasks;

namespace Playdarium.SpreadsheetDownloader.Helpers
{
	public abstract class SingleSpreadsheetDownloader : MultipleSpreadsheetDownloader
	{
		protected abstract string PageId { get; }

		public override sealed string[] PageIds => new[] { PageId };

		protected override sealed async Task Serialize(Dictionary<string, string> jsonByTables)
			=> await Serialize(jsonByTables[PageId]);

		protected abstract Task Serialize(string json);
	}
}