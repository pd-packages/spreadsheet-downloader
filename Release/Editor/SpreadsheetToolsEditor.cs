using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Playdarium.SpreadsheetDownloader.Runtime;
using Playdarium.SpreadsheetDownloader.Utils;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Playdarium.SpreadsheetDownloader
{
	public static class SpreadsheetToolsEditor
	{
		private static CancellationTokenSource _cancellationTokenSource;

		[MenuItem("Tools/Spreadsheets/Download All")]
		public static void DownloadAll()
		{
			var objects = AssemblyUtils.GetObjectsWithSpreadsheetLoaders();

			if (_cancellationTokenSource is { IsCancellationRequested: false })
			{
				_cancellationTokenSource.Cancel();
				_cancellationTokenSource.Dispose();
				_cancellationTokenSource = null;
			}

			_cancellationTokenSource = new CancellationTokenSource();
			DownloadAllBases(objects);
		}

		private static async void DownloadAllBases(IEnumerable<Object> objects)
		{
			var loadedPages = new HashSet<string>();
			using var progressBar = new EditorProgressBar("Spreadsheets");
			progressBar.OnCancel += _cancellationTokenSource.Cancel;

			var spreadsheetLoaders = AssemblyUtils.CreateSpreadsheetLoadersEditors(objects).ToList();
			var loadersCount = spreadsheetLoaders.Count;
			var loaded = 0;

			try
			{
				while (spreadsheetLoaders.Count > 0)
				{
					var hasDownloadPages = false;

					for (var i = 0; i < spreadsheetLoaders.Count; i++)
					{
						var spreadsheetLoader = spreadsheetLoaders[i];
						if (!loadedPages.IsSupersetOf(spreadsheetLoader.DependencyPageIds))
							continue;

						await spreadsheetLoader.DownloadAndSerializeAsync(progressBar, _cancellationTokenSource.Token);
						loadedPages.UnionWith(spreadsheetLoader.PageIds);

						progressBar.Refresh(
							$"Loading {spreadsheetLoader.GetType().Name}...",
							(float)++loaded / loadersCount
						);

						spreadsheetLoaders.RemoveAt(i--);
						hasDownloadPages = true;
					}

					if (!hasDownloadPages && spreadsheetLoaders.Count > 0)
					{
						var loaders = string.Join(", ", spreadsheetLoaders.Select(s => s.GetType().Name));
						throw new Exception($"[{nameof(SpreadsheetToolsEditor)}] Cannot process loaders: {loaders}");
					}
				}
			}
			catch (TaskCanceledException)
			{
				EditorUtility.DisplayDialog("Spreadsheets", "Download CANCELED", "Ok");
			}
			catch (Exception e)
			{
				Debug.LogException(e);
				EditorUtility.DisplayDialog("Spreadsheets", "Download completed with ERROR", "Ok");
			}
			finally
			{
				_cancellationTokenSource.Dispose();
				_cancellationTokenSource = null;
			}

			EditorUtility.DisplayDialog("Spreadsheets", "Download completed SUCCESS", "Ok");
		}
	}
}