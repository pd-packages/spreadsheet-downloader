using System.Threading;
using System.Threading.Tasks;
using Playdarium.SpreadsheetDownloader.Runtime;

namespace Playdarium.SpreadsheetDownloader
{
	public interface ISpreadsheetLoader
	{
		string[] PageIds { get; }

		string[] DependencyPageIds { get; }

		Task DownloadAndSerializeAsync(EditorProgressBar progressBar, CancellationToken cancellationToken);
	}
}