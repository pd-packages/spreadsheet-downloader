using System;

namespace Playdarium.SpreadsheetDownloader.Exceptions
{
	public class SpreadsheetDownloaderCircularDependencyException : Exception
	{
		public SpreadsheetDownloaderCircularDependencyException(string message) : base(message)
		{
		}
	}
}