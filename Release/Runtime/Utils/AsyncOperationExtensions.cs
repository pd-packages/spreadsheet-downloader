using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace Playdarium.SpreadsheetDownloader.Runtime.Utils
{
	[DebuggerNonUserCode]
	public readonly struct AsyncOperationAwaiter<TOperation> : INotifyCompletion
		where TOperation : AsyncOperation
	{
		public readonly TOperation AsyncOperation;

		public bool IsCompleted => AsyncOperation.isDone;

		public AsyncOperationAwaiter(TOperation asyncOperation) => AsyncOperation = asyncOperation;

		public void OnCompleted(Action continuation) => AsyncOperation.completed += _ => continuation();

		public TOperation GetResult() => AsyncOperation;
	}

	public static class AsyncOperationExtensions
	{
		public static AsyncOperationAwaiter<TOperation> GetAwaiter<TOperation>(this TOperation asyncOp)
			where TOperation : AsyncOperation
			=> new(asyncOp);
	}
}